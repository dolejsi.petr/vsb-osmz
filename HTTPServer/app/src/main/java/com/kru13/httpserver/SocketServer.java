package com.kru13.httpserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.Log;

public class SocketServer extends Thread {
	
	ServerSocket serverSocket;
	public final int port = 12345;
	boolean bRunning;
	
	public void close() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			Log.d("SERVER", "Error, probably interrupted in accept(), see log");
			e.printStackTrace();
		}
		bRunning = false;
	}
	
	@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
	public void run() {
        try {
        	Log.d("SERVER", "Creating Socket");
            serverSocket = new ServerSocket(port);
            bRunning = true;
            while (bRunning) {
            	Log.d("SERVER", "Socket Waiting for connection");
                Socket s = serverSocket.accept(); 
                Log.d("SERVER", "Socket Accepted");
                
                OutputStream o = s.getOutputStream();
	        	BufferedWriter out = new BufferedWriter(new OutputStreamWriter(o));
	        	BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

				List<String> request = new ArrayList<String>();
				String tmp;

	            while (!(tmp = in.readLine()).isEmpty()) {
					request.add(tmp);
					Log.d("SERVER CLIENT REQUEST", tmp);
				}

				String fileName = request.get(0).split(" ")[1];
				Log.d("REQUSETED FILE", fileName);

                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/HttpServer" + fileName);

                if (file.exists()) {
                    out.write("HTTP/1.1 200 OK");
                    int i = fileName.lastIndexOf('.');
                    String extension = "";
                    if (i > 0) {
                        extension = fileName.substring(i + 1);
                    }
                    if (extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png") || extension.equals("html") || extension.equals("htm")) {
                        out.write("Connection: close\n");
                        if(extension.equals("png")) out.write("Content-Type: image/png\n");
                        if(extension.equals("jpg") || extension.equals("jpeg")) out.write("Content-Type: image/jpg\n");
                        if(extension.equals("htm") || extension.equals("html")) out.write("Content-Type: text/html\n");
                        out.write("Content-Lenght:" + file.length() + "\n");
                        out.write("\n");
                        out.flush();

                        FileInputStream fis = new FileInputStream(file);
                        byte buffer[] = new byte[1024];
                        int len;
                        while ( (len = fis.read(buffer,0, 1024) ) > 0 ) {
                            o.write(buffer, 0, len );
                        }
                        out.flush();
                        s.close();

                    } else {
                        out.write("\n");
                        out.write("<html><body><h1>Unknown extension</h1></body></html>\n");
                    }
                    out.flush();
                }
                else {
                    out.write("HTTP/1.1 404 Not Found\n");
                    out.write("Content-type: text/html\n");
                    out.write("\n");
                    out.write("<html><body><h1>Error 404</h1></body></html>\n");
                    out.flush();
                }

                s.close();
                Log.d("SERVER", "Socket Closed");
            }
        } 
        catch (IOException e) {
            if (serverSocket != null && serverSocket.isClosed())
            	Log.d("SERVER", "Normal exit");
            else {
            	Log.d("SERVER", "Error");
            	e.printStackTrace();
            }
        }
        finally {
        	serverSocket = null;
        	bRunning = false;
        }
    }

}
